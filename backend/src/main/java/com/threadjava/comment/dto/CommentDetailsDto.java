package com.threadjava.comment.dto;

import com.threadjava.users.dto.UserShortDto;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class CommentDetailsDto {
    private UUID id;
    private String body;
    public long likeCount;
    public long dislikeCount;
    private UserShortDto user;
    private UUID postId;
    private Date createdAt;
    private Boolean isDeleted;

}
