package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.dto.CommentUpdateDto;
import com.threadjava.post.dto.PostUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/comments")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @GetMapping("/{id}")
    public CommentDetailsDto get(@PathVariable UUID id) {
        return commentService.getCommentById(id);
    }

    @PostMapping
    public CommentDetailsDto post(@RequestBody CommentSaveDto commentDto) {
        commentDto.setUserId(getUserId());
        return commentService.create(commentDto);
    }

    @PutMapping
    public CommentDetailsDto update(@RequestBody CommentUpdateDto commentDto) {
        commentDto.setUserId(getUserId());
        return commentService.update(commentDto);
    }

    @PutMapping("/delete")
    public CommentDetailsDto delete(@RequestBody CommentUpdateDto commentDto) {
        commentDto.setUserId(getUserId());
        return commentService.delete(commentDto);
    }
    @GetMapping("/likers/{commentId}")
    public List<PostUserDto> getLikers(@PathVariable UUID commentId) {
        return commentService.getCommentLikers(commentId);
    }
    @GetMapping("/dislikers/{commentId}")
    public List<PostUserDto> getDislikers(@PathVariable UUID commentId) {
        return commentService.getCommentDislikers(commentId);
    }
}
