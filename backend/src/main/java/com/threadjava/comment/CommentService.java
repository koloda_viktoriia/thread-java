package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.dto.CommentUpdateDto;
import com.threadjava.commentReactions.CommentReactionsRepository;
import com.threadjava.commentReactions.model.CommentReaction;
import com.threadjava.post.PostMapper;
import com.threadjava.post.PostsRepository;
import com.threadjava.post.dto.PostUserDto;
import com.threadjava.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private CommentReactionsRepository commentReactionsRepository;
    @Autowired
    private PostsRepository postsRepository;

    public CommentDetailsDto getCommentById(UUID id) {
        return commentRepository.findById(id)
                .map(CommentMapper.MAPPER::commentToCommentDetailsDto)
                .orElseThrow();
    }

    public CommentDetailsDto create(CommentSaveDto commentDto) {
        commentDto.setIsDeleted(false);
        var comment = CommentMapper.MAPPER.commentSaveDtoToModel(commentDto);
        var postCreated = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(postCreated);
    }

    public CommentDetailsDto update(CommentUpdateDto commentDto) {
      var oldComment =  commentRepository.findById(commentDto.getId())
                .map(CommentMapper.MAPPER::commentToCommentDetailsDto)
                .orElseThrow();
      commentDto.setIsDeleted(false);
      commentDto.setCreatedAt(oldComment.getCreatedAt());
       var comment = CommentMapper.MAPPER.commentUpdateDtoToModel(commentDto);
        var postUpdated = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(postUpdated);
    }

    public CommentDetailsDto delete(CommentUpdateDto commentDto) {
        var oldComment =  commentRepository.findById(commentDto.getId())
                .map(CommentMapper.MAPPER::commentToCommentDetailsDto)
                .orElseThrow();
        commentDto.setIsDeleted(true);
        commentDto.setCreatedAt(oldComment.getCreatedAt());
        var comment = CommentMapper.MAPPER.commentUpdateDtoToModel(commentDto);
        var postUpdated = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(postUpdated);
    }
    public List<PostUserDto> getCommentLikers(UUID commentId) {
        return commentReactionsRepository.getLikers(commentId)
                .stream()
                .map(CommentReaction::getUser)
                .map(PostMapper.MAPPER::postUserToPostUserDto)
                .collect(Collectors.toList());
    }

    public List<PostUserDto> getCommentDislikers( UUID commentId) {
        return commentReactionsRepository.getDislikers(commentId)
                .stream()
                .map(CommentReaction::getUser)
                .map(PostMapper.MAPPER::postUserToPostUserDto)
                .collect(Collectors.toList());
    }
}

