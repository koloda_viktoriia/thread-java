package com.threadjava.post;


import com.threadjava.post.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {
    @Autowired
    private PostsService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping
    public List<PostListDto> get(@RequestParam(defaultValue="0") Integer from,
                                 @RequestParam(defaultValue="10") Integer count,
                                 @RequestParam(required = false) UUID userId,
                                 @RequestParam(required = false) Boolean hide,
                                 @RequestParam(required = false) Boolean like) {
        return postsService.getAllPosts(from, count, userId,hide,like);
    }

    @GetMapping("/{id}")
    public PostDetailsDto get(@PathVariable UUID id) {
        return postsService.getPostById(id);
    }

    @PostMapping
    public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.create(postDto);
        template.convertAndSend("/topic/new_post", item);
        return item;
    }

    @PutMapping
    public PostUpdationResponseDto update(@RequestBody PostUpdationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.update(postDto);
        return item;
    }
    @PutMapping("/delete")
    public PostUpdationResponseDto delete(@RequestBody PostUpdationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.delete(postDto);
        return item;
    }
    @GetMapping("/likers/{postId}")
    public List<PostUserDto> getLikers(@PathVariable UUID postId) {
        return postsService.getPostLikers(postId);
    }
    @GetMapping("/dislikers/{postId}")
    public List<PostUserDto> getDislikers(@PathVariable UUID postId) {
        return postsService.getPostDislikers(postId);
    }
}
