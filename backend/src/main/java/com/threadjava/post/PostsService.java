package com.threadjava.post;

import com.threadjava.comment.CommentRepository;
import com.threadjava.post.dto.*;
import com.threadjava.post.model.Post;
import com.threadjava.postReactions.PostReactionsRepository;
import com.threadjava.postReactions.model.PostReaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostsService {
    @Autowired
    private PostsRepository postsCrudRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private PostReactionsRepository postReactionRepository;

    public List<PostListDto> getAllPosts(Integer from, Integer count, UUID userId, Boolean hide, Boolean like) {
        var pageable = PageRequest.of(from / count, count);
        if(hide){
            return postsCrudRepository
                    .findAllNotOwnPosts(userId, pageable)
                    .stream()
                    .map(PostMapper.MAPPER::postListToPostListDto)
                    .collect(Collectors.toList());
        }
        if (like){
            return postsCrudRepository
                    .findAllLiked(userId, pageable)
                    .stream()
                    .map(PostMapper.MAPPER::postListToPostListDto)
                    .collect(Collectors.toList());
        }
        return postsCrudRepository
                .findAllPosts(userId, pageable)
                .stream()
                .map(PostMapper.MAPPER::postListToPostListDto)
                .collect(Collectors.toList());
    }

    public PostDetailsDto getPostById(UUID id) {
        var post = postsCrudRepository.findPostById(id)
                .map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow();

        var comments = commentRepository.findAllByPostId(id)
                .stream()
                .map(PostMapper.MAPPER::commentToCommentDto)
                .collect(Collectors.toList());
        post.setComments(comments);

        return post;
    }

    public PostCreationResponseDto create(PostCreationDto postDto) {
        postDto.setIsDeleted(false);
        Post post = PostMapper.MAPPER.postDetailsDtoToPost(postDto);
        Post postCreated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostCreationResponseDto(postCreated);
    }
    public PostUpdationResponseDto update(PostUpdationDto postDto) {
        var oldPost = postsCrudRepository.findPostById(postDto.getId())
                .map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow();
        postDto.setCreatedAt(oldPost.getCreatedAt());
        postDto.setIsDeleted(false);
        Post post = PostMapper.MAPPER.postDetailsDtoToPost(postDto);
        Post postUpdated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostUpdationResponseDto(postUpdated);
    }
    public PostUpdationResponseDto delete(PostUpdationDto postDto) {
        var oldPost = postsCrudRepository.findPostById(postDto.getId())
                .map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow();
        postDto.setCreatedAt(oldPost.getCreatedAt());
        Post post = PostMapper.MAPPER.postDetailsDtoToPost(postDto);
        Post postUpdated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostUpdationResponseDto(postUpdated);
    }

    public List<PostUserDto> getPostLikers( UUID postId) {
         return postReactionRepository.getLikers(postId)
                     .stream()
                     .map(PostReaction::getUser)
                     .map(PostMapper.MAPPER::postUserToPostUserDto)
                     .collect(Collectors.toList());

    }

    public List<PostUserDto> getPostDislikers( UUID postId) {
        return postReactionRepository.getDislikers(postId)
                    .stream()
                    .map(PostReaction::getUser)
                    .map(PostMapper.MAPPER::postUserToPostUserDto)
                    .collect(Collectors.toList());
           }
}


