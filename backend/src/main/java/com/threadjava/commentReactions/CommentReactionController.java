package com.threadjava.commentReactions;

import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/commentreactions")
public class CommentReactionController {
    @Autowired
    private CommentReactionService commentsService;
    @Autowired
    private SimpMessagingTemplate template;

    @PutMapping
    public Optional<ResponseCommentReactionDto> setReaction(@RequestBody ReceivedCommentReactionDto commentReaction){
        commentReaction.setUserId(getUserId());
        var reaction = commentsService.setReaction(commentReaction);
        return reaction;
    }



    @GetMapping("/{commentId}")
    public Optional<ResponseCommentReactionDto> get(@PathVariable UUID commentId) {
        var reaction = commentsService.getCommentReaction(getUserId(),commentId);
        return reaction;
    }

}
