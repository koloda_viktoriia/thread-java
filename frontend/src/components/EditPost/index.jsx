import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Segment } from 'semantic-ui-react';

import styles from './styles.module.scss';

const EditPost = ({
  post,
  editPost,
  uploadImage,
  close
}) => {
  const {
    id,
    image,
    body
  } = post;
  const [bodyEdit, setBody] = useState(body);
  const [imageEdit, setImage] = useState({ ImageId: image?.id, imageLink: image?.link });
  const [isUploading, setIsUploading] = useState(false);
  const handleEditPost = async () => {
    if (!bodyEdit) {
      return;
    }
    await editPost({ id, imageId: imageEdit?.imageId, imageLink: imageEdit?.imageLink, body: bodyEdit });
    await close();
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      setIsUploading(false);
    }
  };
  return (
    <Segment>
      <Form onSubmit={handleEditPost}>
        <Form.TextArea
          name="bodyEdit"
          value={bodyEdit}
          onChange={ev => setBody(ev.target.value)}
        />
        {imageEdit?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={imageEdit?.imageLink} alt="post" />
          </div>
        )}
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Attach image
          <input name="imageEdit" type="file" onChange={handleUploadFile} hidden />
        </Button>
        <Button floated="right" color="blue" type="submit">Edit</Button>
      </Form>
    </Segment>
  );
};

EditPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  editPost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default EditPost;
