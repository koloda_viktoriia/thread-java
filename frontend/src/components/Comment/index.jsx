import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon, Label, Popup, PopupContent, List } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import * as commentService from 'src/services/commentService';

import styles from './styles.module.scss';

const Comment = ({
  comment,
  userId,
  likeComment,
  dislikeComment,
  editComment,
  deleteComment }) => {
  const [likers, setLikers] = useState('');
  const [dislikers, setDislikers] = useState('');
  const { id,
    body,
    likeCount,
    dislikeCount,
    createdAt,
    user,
    isDeleted } = comment;

  const showLikers = async () => {
    const result = await commentService.getLikers(id);
    setLikers(result);
  };

  const showDislikers = async () => {
    const result = await commentService.getDislikers(id);
    setDislikers(result);
  };

  if (isDeleted !== true) {
    return (
      <CommentUI className={styles.comment}>
        <CommentUI.Avatar src={getUserImgLink(user.image)} />
        <CommentUI.Content>
          <CommentUI.Author as="a">
            {user.username}
          </CommentUI.Author>
          <CommentUI.Metadata>
            {moment(createdAt).fromNow()}
          </CommentUI.Metadata>
          {userId === user.id && (
            <Label basic size="small" as="a" className={styles.toolsBtn} onClick={() => editComment(id)}>
              <Icon name="edit" />
            </Label>
          )}
          {userId === user.id && (
            <Label basic size="small" as="a" className={styles.toolsBtn} onClick={() => deleteComment(id)}>
              <Icon name="trash" />
            </Label>
          )}
          <CommentUI.Text>
            {body}
          </CommentUI.Text>
        </CommentUI.Content>
        <CommentUI.Content extra>
          <Popup
            onOpen={() => showLikers()}
            pinned
            trigger={(
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => likeComment(id)}
              >
                <Icon name="thumbs up" />
                {likeCount}
              </Label>
            )}
          >
            <PopupContent>
              <List>
                {likers.length !== 0
                  ? likers.map(liker => (
                    <List.Item
                      content={liker.username}
                      key={liker.id}
                    />
                  )) : (
                    <List.Item
                      content="Nobody liked comment yet."
                    />
                  )}
              </List>
            </PopupContent>
          </Popup>
          <Popup
            onOpen={() => showDislikers()}
            pinned
            trigger={(
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => dislikeComment(id)}
              >
                <Icon name="thumbs down" />
                {dislikeCount}
              </Label>
            )}
          >
            <PopupContent>
              <List>
                {dislikers.length !== 0
                  ? dislikers.map(disliker => (
                    <List.Item
                      content={disliker.username}
                      key={disliker.id}
                    />
                  )) : (
                    <List.Item
                      content="Nobody disliked comment yet."
                    />
                  )}
              </List>
            </PopupContent>
          </Popup>
        </CommentUI.Content>
      </CommentUI>
    );
  }
  return (null);
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  editComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired
};

export default Comment;
