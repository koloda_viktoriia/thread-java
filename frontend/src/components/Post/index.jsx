import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Popup, PopupContent, List, Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import * as postService from 'src/services/postService';
import styles from './styles.module.scss';

const Post = ({
  post,
  likePost,
  dislikePost,
  toggleExpandedPost,
  editPost,
  deletePost,
  sharePost,
  userId }) => {
  const [likers, setLikers] = useState('');
  const [dislikers, setDislikers] = useState('');
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    deleted
  } = post;
  const date = moment(createdAt).fromNow();
  const showLikers = async () => {
    const result = await postService.getLikers(id);
    setLikers(result);
  };

  const showDislikers = async () => {
    const result = await postService.getDislikers(id);
    setDislikers(result);
  };

  if (deleted !== true) {
    return (
      <Card style={{ width: '100%' }}>
        {image && <Image src={image.link} wrapped ui={false} />}
        <Card.Content>
          <Card.Meta>
            <span className="date">
              posted by
              {' '}
              {user.username}
              {' - '}
              {date}
            </span>
            {userId === user.id && (
              <Label basic size="small" as="a" className={styles.toolsBtn} onClick={() => editPost(id)}>
                <Icon name="edit" />
              </Label>
            )}
            {userId === user.id && (
              <Label basic size="small" as="a" className={styles.toolsBtn} onClick={() => deletePost(id)}>
                <Icon name="trash" />
              </Label>
            )}
          </Card.Meta>
          <Card.Description>
            {body}
          </Card.Description>
        </Card.Content>
        <Card.Content extra>
          <Popup
            onOpen={() => showLikers()}
            pinned
            trigger={(
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => likePost(id)}
              >
                <Icon name="thumbs up" />
                {likeCount}
              </Label>
            )}
          >
            <PopupContent>
              <List>
                {likers.length !== 0
                  ? likers.map(liker => (
                    <List.Item
                      content={liker.username}
                      key={liker.id}
                    />
                  )) : (
                    <List.Item
                      content="Nobody liked post yet."
                    />
                  )}
              </List>
            </PopupContent>
          </Popup>
          <Popup
            onOpen={() => showDislikers()}
            pinned
            trigger={(
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => dislikePost(id)}
              >
                <Icon name="thumbs down" />
                {dislikeCount}
              </Label>
            )}
          >
            <PopupContent>
              <List>
                {dislikers.length !== 0
                  ? dislikers.map(disliker => (
                    <List.Item
                      content={disliker.username}
                      key={disliker.id}
                    />
                  )) : (
                    <List.Item
                      content="Nobody disliked post yet."
                    />
                  )}
              </List>
            </PopupContent>
          </Popup>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
            <Icon name="comment" />
            {commentCount}
          </Label>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
            <Icon name="share alternate" />
          </Label>

        </Card.Content>
      </Card>
    );
  }
  return (null);
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired
};

export default Post;
