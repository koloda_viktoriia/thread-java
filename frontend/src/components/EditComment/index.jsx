import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';

const EditComment = ({
  comment,
  postId,
  editComment,
  close
}) => {
  const [bodyEdit, setBody] = useState(comment.body);

  const handleEditComment = async () => {
    if (!bodyEdit) {
      return;
    }
    await editComment({ id: comment.id, postId, body: bodyEdit });
    await close();
  };

  return (
    <Form reply onSubmit={handleEditComment}>
      <Form.TextArea
        value={bodyEdit}
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type="submit" content="Edit comment" labelPosition="left" icon="edit" primary />
    </Form>
  );
};

EditComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  postId: PropTypes.string.isRequired,
  editComment: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default EditComment;
