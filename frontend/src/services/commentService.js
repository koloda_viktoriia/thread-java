import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'POST',
    request
  });
  return response.json();
};

export const editComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'PUT',
    request
  });
  return response.json();
};

export const deleteComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments/delete',
    type: 'PUT',
    request
  });
  return response.json();
};

export const getComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const likeComment = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/commentreactions',
    type: 'PUT',
    request: {
      commentId,
      isLike: true
    }
  });
  return response.json();
};
export const dislikeComment = async commentId => {
  const response = await callWebApi({
    endpoint: '/api/commentreactions',
    type: 'PUT',
    request: {
      commentId,
      isLike: false
    }
  });
  return response.json();
};

export const getCommentReaction = async commentId => {
  const response = await callWebApi({
    endpoint: `/api/commentreactions/${commentId}`,
    type: 'GET'
  });
  return response.json();
};

export const getLikers = async commentId => {
  const response = await callWebApi({
    endpoint: `/api/comments/likers/${commentId}`,
    type: 'GET'
  });
  return response.json();
};

export const getDislikers = async commentId => {
  const response = await callWebApi({
    endpoint: `/api/comments/dislikers/${commentId}`,
    type: 'GET'
  });
  return response.json();
};
