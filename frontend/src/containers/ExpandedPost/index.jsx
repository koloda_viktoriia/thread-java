import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import EditedComment from 'src/containers/EditedComment';

import moment from 'moment';
import {
  likePost,
  dislikePost,
  likeComment,
  dislikeComment,
  toggleExpandedPost,
  addComment,
  editComment,
  deleteComment,
  deletePost
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  post,
  sharePost,
  editPost,
  likePost: like,
  dislikePost: dislike,
  likeComment: clike,
  dislikeComment: cdislike,
  toggleExpandedPost: toggle,
  addComment: add,
  editComment: edit,
  userId,
  deletePost: deleted,
  deleteComment: del
}) => {
  const [editedCommentId, setEditedCommentId] = useState(undefined);
  const editedComment = commentId => {
    setEditedCommentId(commentId);
  };
  if (post?.isDeleted !== true) {
    return (
      <Modal centered={false} open onClose={() => toggle()}>
        {post
          ? (
            <Modal.Content>
              <Post
                post={post}
                likePost={like}
                dislikePost={dislike}
                toggleExpandedPost={toggle}
                sharePost={sharePost}
                editPost={editPost}
                userId={userId}
                deletePost={deleted}
              />
              <CommentUI.Group style={{ maxWidth: '100%' }}>
                <Header as="h3" dividing>
                  Comments
                </Header>
                {post.comments && post.comments
                  .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                  .map(comment => (
                    <Comment
                      key={comment.id}
                      comment={comment}
                      userId={userId}
                      likeComment={clike}
                      dislikeComment={cdislike}
                      editComment={editedComment}
                      deleteComment={del}
                    />
                  ))}
                <AddComment postId={post.id} addComment={add} />
              </CommentUI.Group>
              {editedCommentId && (
                <EditedComment
                  comments={post.comments}
                  commentId={editedCommentId}
                  postId={post.id}
                  close={() => setEditedCommentId(undefined)}
                  editComment={edit}
                />
              )}
            </Modal.Content>

          )
          : <Spinner />}
      </Modal>
    );
  }
  return (null);
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  editComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost
});

const actions = {
  likePost,
  dislikePost,
  likeComment,
  dislikeComment,
  toggleExpandedPost,
  addComment,
  editComment,
  deleteComment,
  deletePost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
