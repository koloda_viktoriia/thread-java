import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'semantic-ui-react';
import EditPost from 'src/components/EditPost';
import * as imageService from 'src/services/imageService';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { editPost } from 'src/containers/Thread/actions';

import styles from './styles.module.scss';

const EditedPost = ({
  posts,
  postId,
  close,
  editPost: updatePost
}) => {
  const uploadImage = file => imageService.uploadImage(file);
  const post = posts.find(item => item.id === postId);
  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Edit Post</span>
      </Modal.Header>
      <Modal.Content>
        <EditPost post={post} postId={postId} editPost={updatePost} uploadImage={uploadImage} close={close} />
      </Modal.Content>
    </Modal>
  );
};

EditedPost.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired
};

EditedPost.defaultProps = {
  posts: []
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts
});

const actions = { editPost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditedPost);

