import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const editPost = request => async (dispatch, getRootState) => {
  const editedPost = await postService.editPost(request);
  const mapEditedPost = post => ({
    ...post,
    body: request.body,
    image: { id: request.imageId, link: request.imageLink }
  });
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== editedPost.id ? post : mapEditedPost(post)));
  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === editedPost.id) {
    dispatch(setExpandedPostAction(mapEditedPost(expandedPost)));
  }
};

export const deletePost = postId => async (dispatch, getRootState) => {
  const { id, imageId, imageLink, body } = await postService.getPost(postId);
  const request = { id, imageId, imageLink, body, isDeleted: true };
  const { isDeleted } = await postService.deletePost(request);
  const mapDeletedPost = post => ({
    ...post,
    deleted: isDeleted
  });
  const mapDeletedExpandedPost = post => ({
    ...post,
    isDeleted
  });
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDeletedPost(post)));
  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDeletedExpandedPost(expandedPost)));
  }
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const reaction = await postService.getPostReaction(postId);
  const result = await postService.likePost(postId);
  const diffLikes = result?.id ? 1 : -1;
  const diffDislikes = (reaction?.isLike === false) ? -1 : 0;
  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diffLikes,
    dislikeCount: Number(post.dislikeCount) + diffDislikes
  });
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};
export const dislikePost = postId => async (dispatch, getRootState) => {
  const reaction = await postService.getPostReaction(postId);
  const result = await postService.dislikePost(postId);
  const diffDislikes = result?.id ? 1 : -1;
  const diffLikes = (reaction?.isLike === true) ? -1 : 0;
  const mapDislikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diffLikes,
    dislikeCount: Number(post.dislikeCount) + diffDislikes
  });
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};
export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);
  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const editComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.editComment(request);
  const comment = await commentService.getComment(id);
  const mapComments = post => {
    const { comments } = post;
    const editedComments = comments.map(item => (item.id === comment.id ? comment : item));
    return ({
      ...post,
      comments: editedComments
    });
  };
  const { posts: { expandedPost } } = getRootState();

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const deleteComment = commentId => async (dispatch, getRootState) => {
  const { id, body, postId } = await commentService.getComment(commentId);
  const request = { id, body, postId };
  const comment = await commentService.deleteComment(request);
  const mapComments = post => {
    const { comments } = post;
    const editedComments = comments.map(item => (item.id === comment.id ? comment : item));
    return ({
      ...post,
      commentCount: Number(post.commentCount) - 1,
      comments: editedComments
    });
  };
  const mapCommentsCount = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1
  });
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapCommentsCount(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  const { postId } = await commentService.getComment(commentId);
  const reaction = await commentService.getCommentReaction(commentId);
  const result = await commentService.likeComment(commentId);
  const diffLikes = result?.id ? 1 : -1;
  const diffDislikes = (reaction?.isLike === false) ? -1 : 0;
  const mapLikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diffLikes,
    dislikeCount: Number(comment.dislikeCount) + diffDislikes
  });
  const mapComments = post => {
    const { comments } = post;
    const editedComments = comments.map(comment => (comment.id === commentId ? mapLikes(comment) : comment));
    return ({
      ...post,
      comments: editedComments
    });
  };
  const { posts: { expandedPost } } = getRootState();

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};
export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const { postId } = await commentService.getComment(commentId);
  const reaction = await commentService.getCommentReaction(commentId);
  const result = await commentService.dislikeComment(commentId);
  const diffDislikes = result?.id ? 1 : -1;
  const diffLikes = (reaction?.isLike === true) ? -1 : 0;
  const mapDislikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diffLikes,
    dislikeCount: Number(comment.dislikeCount) + diffDislikes
  });
  const mapComments = post => {
    const { comments } = post;
    const editedComments = comments.map(comment => (comment.id === commentId ? mapDislikes(comment) : comment));
    return ({
      ...post,
      comments: editedComments
    });
  };
  const { posts: { expandedPost } } = getRootState();

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

