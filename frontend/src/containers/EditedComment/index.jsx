import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'semantic-ui-react';
import EditComment from 'src/components/EditComment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './styles.module.scss';

const EditedComment = ({
  comments,
  postId,
  commentId,
  close,
  editComment: updateComment
}) => {
  const comment = comments.find(item => item.id === commentId);
  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Edit Comment</span>
      </Modal.Header>
      <Modal.Content>
        <EditComment comment={comment} postId={postId} editComment={updateComment} close={close} />
      </Modal.Content>
    </Modal>
  );
};

EditedComment.propTypes = {
  comments: PropTypes.arrayOf(PropTypes.object).isRequired,
  postId: PropTypes.string.isRequired,
  commentId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  editComment: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => bindActionCreators(dispatch);
export default connect(
  mapDispatchToProps
)(EditedComment);

